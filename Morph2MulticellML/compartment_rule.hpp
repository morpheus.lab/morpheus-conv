#include <QtCore>
#include <QtXml>
#include <QDebug>

#include "multicell_model.hpp"
#include "../rule_base.hpp"

namespace Morph2MultiCell {
  
class CompartmentRule :public XMLRuleBase
{
	Model& model;
	QDomElement compartment_list;
	int ct_id = 1;
	bool medium_defined = false;

	public:
		CompartmentRule(Model& a) : model(a) {};
		QList<MatchDesc> matchPaths() override {
			return {
				{ "Global", CONSUMING_NODE},
				{ "CellTypes/CellType", CONSUMING_NODE}
			};
		}
		
		bool matches(const QDomElement node) { for (auto match : matchPaths()) { if (matchPath(match.path,node)) return true; } return false; };
		void enterMatchingNode(const QDomElement ) override;
		void leaveMatchingNode(const QDomElement node) override;
		void finish() override;
	private:
};

void CompartmentRule::leaveMatchingNode(const QDomElement node) {
	if ( ! matches(node) ) return;
	model.leaveScope();
}

void CompartmentRule::enterMatchingNode(const QDomElement node)
{
	QString id;
	QDomElement compartment_node ;
	if (node.nodeName() == "Global") {
		id = "global";
		auto global_scope = model.rootScope();
		compartment_node = model.getDocument().createElement("Global");
		
		model.addCompartment(model.rootScope());
		global_scope->setMLNode(compartment_node);
		model.enterScope(global_scope);
		model.getModelRoot().appendChild(compartment_node);
	}
	else {
		id = node.attribute("name");
		compartment_node = model.getDocument().createElement("CompartmentType");
		compartment_node.setAttribute("id",id);
		compartment_node.setAttribute("spatial", "true");
		
		if (node.attribute("class")=="medium") {
			compartment_node.setAttribute("prototype", "Fluid");
		}
		else if (node.attribute("class")=="biological") {
			if (node.firstChildElement("FreezeMotion").isNull()) {
				compartment_node.setAttribute("prototype", "Cell");
			}
			else {
				compartment_node.setAttribute("prototype", "Solid");
			}
		}
		else { throw; }
		auto compartment_scope = model.currentScope()->createSubScope(compartment_node,id);
		model.addCompartment(compartment_scope);
		model.enterScope(compartment_scope);
		if (compartment_list.isNull()){
			compartment_list = model.getDocument().createElement("ListOfCompartmentTypes");
			model.getModelRoot().appendChild(compartment_list);
		}
			
		compartment_list.appendChild(compartment_node);
	}
}

void CompartmentRule::finish() {
	/// Add all CompartmentTypes to be subtypes of Global and if there is a LiquidType make them a space-tiling set
	auto comps = model.getCompartmentsByName();
	auto global_scope  = comps["global"];
	if (!global_scope) {
		global_scope = model.rootScope();
		auto compartment_node = model.getDocument().createElement("Global");
		compartment_list.appendChild(compartment_node);
		global_scope->setMLNode(compartment_node);
		model.addCompartment(global_scope);
		comps["global"] = global_scope;
	}
	
	auto global = global_scope->getMLNode();
	bool have_medium = false;
	for (auto comp : comps) {
		if (comp->getCompartmentType() == CompartmentType::Global)
			continue;
		setPath(global, QString("ListOfSubCompartmentTypes/CompartmentType[ref='%1']/@text").arg(comp->getCompartmentName()),"");
		if (comp->getCompartmentType() == CompartmentType::Liquid) {
			// Morpheus assumes the first liquid type to serve as medium
			have_medium = true;
			setPath(global, QString("ListOfSubCompartmentTypes/@space-tiling"),"true");
			break;
		}
	}
}

}
