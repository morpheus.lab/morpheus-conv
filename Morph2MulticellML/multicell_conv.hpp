#ifndef MULTICELL_CONV_HPP
#define MULTICELL_CONV_HPP

#include "multicell_model.hpp"


/// Convert a MorpheusML document to CC3D
QDomDocument MorpheusML_to_MulticellML(QDomDocument morpheus_doc);

#endif
