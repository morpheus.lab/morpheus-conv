#ifndef CC3D_MODEL_HPP
#define CC3D_MODEL_HPP

#include <QtCore>
#include <QtXml>
#include <QDebug>
#include <QWeakPointer>
#include "../rule_base.hpp"

namespace Morph2MultiCell {

enum class CompartmentType { Liquid, Solid, Cell, Global };
QMap<QString, CompartmentType> getCompartmentTypeMap();

class Scope {
public:
	Scope(QDomElement MLNode) : parent(nullptr), ml_node(MLNode),  id("global"), type(CompartmentType::Global) {};
	QSharedPointer<Scope> createSubScope(QDomElement MLNode, QString name = "") { auto subscope = QSharedPointer<Scope>( new Scope(MLNode,this) ); return subscope; }
	Scope* getParent() const { return parent; } 
	QDomElement getMLNode() const { return ml_node; }
	void setMLNode(QDomElement  compartment_node) { ml_node = compartment_node; } 
	CompartmentType getCompartmentType() const { return type; }
	QString getCompartmentName() const { return id; };
	
	bool setPath(const QString path, const QString value) { return XMLRuleBase::setPath(ml_node, path, value); };
	bool setPath(const QString path, int value) { return setPath(path,QString::number(value)); };

private:
	Scope(QDomElement MLNode, Scope* p) : ml_node(MLNode), parent(p) {
		if (getCompartmentTypeMap().contains(MLNode.attribute("prototype")))
			type = getCompartmentTypeMap()[MLNode.nodeName()];
		else {
			qDebug() << "Unknown compartment type " << MLNode.attribute("prototype");
			throw;
		}
		id = ml_node.attribute("id");
	};
	Scope* parent;
	QDomElement ml_node;
	QString id;
	CompartmentType type;
	QList<QSharedPointer<Scope>> sub_scopes;
};


/// The Model provides parsing if the fundamental information of the input model to be used by the converter rules
class Model
{
	QDomDocument document;
	QSharedPointer<Scope> root, current_scope;
	QList<QSharedPointer<Scope>> scope_stash;
	QMap<QString, QSharedPointer<Scope>> compartments;
	
	QString lattice_type;
	double lattice_node_length;
	int lattice_dim;

	public:
		Model(const QDomDocument& morpheus_model);
		/// Get the CC3D XML model description
		QDomDocument getDocument() const { return document; }
		QDomElement getModelRoot() const { return document.firstChildElement().firstChildElement("Model");}
		QDomElement getSimulationRoot() const { return document.firstChildElement().firstChildElement("Simulation");}
		/// Get the name of current CellType
		QString currentCompartment() const { return current_scope->getCompartmentName(); } 
		void addCompartment(QSharedPointer<Scope> compartment) { compartments.insert(compartment->getCompartmentName(), compartment); }
		const QMap<QString, QSharedPointer<Scope>>& getCompartmentsByName() const { return compartments; };
// 		void setMediumCellType(QString type) { medium_cell_type = type; }
// 		QString translateCellType(QString celltype) { if (celltype == medium_cell_type) return "Medium"; else if (celltype.isEmpty()) return cellType; else return celltype; } 
		
		QString getLatticeType() const { return lattice_type; }
		int getLatticeDimensions() const { return lattice_dim; }
		double getNodeLength() const { return lattice_node_length; }
		
		QString getOptimalNeighborhoodOrder() const;
// 		QString neighborhoodDistance2Order(QString distance);
		
		/// Set the current CellType
		void enterScope(QSharedPointer<Scope> scope) { scope_stash.append(current_scope); current_scope = scope; };
		void leaveScope() { current_scope = scope_stash.takeLast(); };
		/// Get the current Node of the document
		QSharedPointer<Scope> currentScope() { return current_scope; }
		/// Get the Root Scope of the document
		QSharedPointer<Scope> rootScope() { return root; }
};

};

#endif
