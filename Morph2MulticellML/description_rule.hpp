#include <QtCore>
#include <QtXml>
#include <QDebug>

#include "multicell_model.hpp"
#include "../rule_base.hpp"

namespace Morph2MultiCell {

class DescriptionRule :public XMLRuleBase
{
	Model& model;
	int ct_id = 0;

	public:
		DescriptionRule(Model& a) : model(a) {};
		QList<MatchDesc> matchPaths() override {
			return {
				{ "Description/Title",  CONSUMING_NODE},
				{ "Description/Author", CONSUMING_NODE},
				{ "Description/Date",   CONSUMING_NODE},
				{ "Description/DOI",    CONSUMING_NODE},
				{ "Description/Details",CONSUMING_NODE}
			};
		}
		
		void enterMatchingNode(const QDomElement) override;
		void finish() override {};
	private:
		
};

void DescriptionRule::enterMatchingNode(const QDomElement node)
{
	if (node.nodeName() == "Title") {
		setPath(model.getModelRoot(), "Description/Title", node.text());
	}
	else if (node.nodeName() == "Author") {
		setPath(model.getModelRoot(), "Description/Author", node.text());
	}
	else if (node.nodeName() == "Date") {
		setPath(model.getModelRoot(), "Description/Date", node.text());
	}
	else if (node.nodeName() == "DOI") {
		setPath(model.getModelRoot(), "Description/DOI", node.text());
	}
	else if (node.nodeName() == "Details") {
		setPath(model.getModelRoot(), "Description/Details", node.text());
	}
}

}

