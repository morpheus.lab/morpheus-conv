#include "multicell_model.hpp"
namespace Morph2MultiCell {
	
QMap<QString, CompartmentType> getCompartmentTypeMap() {
	return {{"LiquidType"  ,CompartmentType::Liquid},
	        {"SolidType"   ,CompartmentType::Solid},
	        {"CellType"    ,CompartmentType::Cell},
	        {"Global"      ,CompartmentType::Global},
	        {"Fluid"       ,CompartmentType::Liquid},
	        {"Solid"       ,CompartmentType::Solid},
	        {"Cell"        ,CompartmentType::Cell},
	        {"Global"      ,CompartmentType::Global}
	};
}
	
Model::Model(const QDomDocument& morpheus_model)
{
	document = QDomDocument("");
	QString error_msg; int line, col;
	
	QFile minimal_file(":/multicell/multicell_minimal.xml");
	document.setContent(&minimal_file, &error_msg, &line, &col);
	
	root = QSharedPointer<Scope>::create(document.firstChildElement().firstChildElement("Model"));

	auto morpheus_root = morpheus_model.firstChildElement();
	
	
	auto lattice = morpheus_root.firstChildElement("Space").firstChildElement("Lattice");
	auto lattice_type = lattice.attribute("class");
	
	if (lattice_type == "linear") { lattice_dim = 1; }
	else if (lattice_type == "square") { lattice_dim = 2; }
	else if (lattice_type == "hexagonal") { lattice_dim = 2; }
	else if (lattice_type == "cubic") { lattice_dim = 3; }
	else lattice_dim = 3;
	
	auto size = XMLRuleBase::getPath(lattice,"Size").attribute("value").split(",");
	
	auto node_length_node = XMLRuleBase::getPath(lattice,"NodeLength");
	
	if (!node_length_node.isNull()) 
		lattice_node_length = node_length_node.attribute("value").toDouble();
	else 
		lattice_node_length = 1.0;
	
	for (int i=0; i<3; i++) {
		if (i>=lattice_dim) size[i] = QString::number(0);
		else size[i] = QString::number( size[i].trimmed().toDouble() * lattice_node_length );
	}
		
	root->setPath("Model/Space/Size/@value", size.join(","));
	root->setPath("Model/Space/Dimension/@text", lattice_dim);
	root->setPath("Simulation/Lattice/NodeLength/@value",QString::number(lattice_node_length));
	root->setPath("Simulation/Lattice/@class", lattice_type);
}

} // namespace Multicell
