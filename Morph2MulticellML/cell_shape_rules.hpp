#include <QtCore>
#include <QtXml>
#include <QDebug>

#include "multicell_model.hpp"
#include "../rule_base.hpp"

namespace Morph2MultiCell {

class CellShapeRule :public XMLRuleBase
{
	Model& model;
	int ct_id = 0;

	public:
		CellShapeRule(Model& a) : model(a) {};
		QList<MatchDesc> matchPaths() override { 
			return { 
				{"VolumeConstraint",  XMLRuleBase::CONSUMING_NODE},
				{"SurfaceConstraint", XMLRuleBase::CONSUMING_NODE} 
			};
		}
		void enterMatchingNode(const QDomElement) override;
		void finish() override {};
	private:
		
};

void CellShapeRule::enterMatchingNode(const QDomElement node)
{
	if (matchPath("VolumeConstraint",node)) {

		setPath(model.getModelRoot(),
				QString("ListOfSpatialCompartmentTypes/CellType[id=%1]/Volume/cpm::VolumeConstraint/@strength").arg(model.currentCompartment()), 
				node.attribute("strength"));
		setPath(model.getModelRoot(),
				QString("ListOfSpatialCompartmentTypes/CellType[id=%1]/Volume/@value").arg(model.currentCompartment()),
				QString::number(node.attribute("target").toDouble() * pow(model.getNodeLength(), model.getLatticeDimensions())) );
	}
	if (matchPath("SurfaceConstraint",node)) {
		
		setPath(model.getModelRoot(),
				QString("ListOfSpatialCompartmentTypes/CellType[id=%1]/Surface/cpm::SurfaceConstraint/@strength").arg(model.currentCompartment()), 
				node.attribute("strength"));
		setPath(model.getModelRoot(),
				QString("ListOfSpatialCompartmentTypes/CellType[id=%1]/Surface/@value").arg(model.currentCompartment()),
				node.attribute("target"));
		setPath(model.getModelRoot(),
				QString("ListOfSpatialCompartmentTypes/CellType[id=%1]/Surface/@mode").arg(model.currentCompartment()),
				node.attribute("mode"));
	}
}

}
