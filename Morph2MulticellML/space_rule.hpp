#include "multicell_model.hpp"
#include "../rule_base.hpp"

namespace Morph2MultiCell {
  
class SpaceRule : public XMLRuleBase {
	public: 
		SpaceRule(Model model ) : model(model)  {};
		QList<MatchDesc> matchPaths() override { 
			return { 
				{ "Lattice", CONSUMING_SUBTREE },
				{ "SpaceSymbol", CONSUMING_NODE },
				{ "TimeSymbol", CONSUMING_NODE }
			};
		}
		void enterMatchingNode(const QDomElement node) override;
		void finish() override {};

	private: 
		Model model;
		double node_length = 1.0;
		QStringList boundary_conditions;
		QMap<QString,int> boundary_idx = {{"-x",0},{"x",1},{"-y",2},{"y",3},{"-z",4},{"z",5}};
};

void SpaceRule::enterMatchingNode(const QDomElement node)
{
	if (node.tagName() == "Lattice") {
		
		// Lattice structure and size are parsed by the output model itself
		// in order to provide some of that data in the interface
		
		// default initialize boundaries
		
		auto dim = model.getLatticeDimensions();
		for (int i=0; i<dim*2; i++) boundary_conditions.append("periodic");
		// parse boundary overrides
		auto boundary_node = getPath(node,"BoundaryConditions");
		if (!boundary_node.isNull()) {
			auto constraints = boundary_node.childNodes();
			for ( int i=0; i<constraints.size(); i++) {
				if (constraints.at(i).nodeName() == "Constraint") {
					auto bdry = constraints.at(i).toElement().attribute("boundary");
					auto type = constraints.at(i).toElement().attribute("type");
					boundary_conditions[boundary_idx[bdry]] = type;
				}
			}
		}
		// write out boundaries
		for (uint i=0; i<boundary_conditions.size(); i++) {
			setPath(model.getModelRoot(),QString("Space/ListOfBoundaryConditions/Condition[boundary=%1]/@type").arg(boundary_idx.key(i,"missing")), boundary_conditions[i]);
		}
	}
	if (node.tagName() == "SpaceSymbol") {
		setPath(model.getModelRoot(),"Space/@symbol", node.attribute("symbol"));
	}
	if (node.tagName() == "TimeSymbol") {
		setPath(model.getModelRoot(),"Time/@symbol", node.attribute("symbol"));
	}
}

}
