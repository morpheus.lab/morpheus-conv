#include <QtCore>
#include <QtXml>
#include <QDebug>

#include "multicell_model.hpp"
#include "../rule_base.hpp"

namespace Morph2MultiCell {

class PottsRule : public XMLRuleBase
{
	private:
		QStringList dim;
		QString latticeType;
		QString neighbourhoodOrder;
		QString neighbourhoodDistance;
		bool cpm_defined = false;
		bool metropolis_defined = false;
		
		Model& model;

	public:
		PottsRule(Model& a) : model(a) {};
		QList<MatchDesc> matchPaths() override { 
			return {
				{ "Lattice/Neighborhood", USING_NODE },
				{ "CPM", CONSUMING_SUBTREE },
				{ "StartTime", CONSUMING_NODE },
				{ "StopTime", CONSUMING_NODE },
				{ "MonteCarloSampler/MetropolisKinetics", CONSUMING_SUBTREE },
				{ "MonteCarloSampler/Neighborhood", CONSUMING_SUBTREE },
				{ "MonteCarloSampler/MCSDuration", CONSUMING_SUBTREE},
				{ "ShapeSurface", CONSUMING_SUBTREE }
			};
		}
		void enterMatchingNode(const QDomElement) override;
		void finish() override;
};


void PottsRule::enterMatchingNode(const QDomElement node)
{
	QDomElement elem=node;

// TODO the variable names in these two groups collide with later CPM-specific neighborhood definitions -> maybe this makes sense as defaults	
	if (matchPath("Lattice/Neighborhood", node) ) {
		auto child = node.firstChildElement("Order");
		if (!child.isNull()) {
			neighbourhoodOrder = child.text();
			return;
		}
		child =  node.firstChildElement("Distance");
		if (!child.isNull()) {
			neighbourhoodDistance = child.text();
			return;
		}
	}
	else if (matchPath("CPM", node)) {
		cpm_defined = true;
	}
	else if (matchPath("StartTime",node)) {
		setPath(model.getSimulationRoot(),"StartTime/@value", node.attribute("value"));
	}
	else if (matchPath("StopTime",node)) {
		setPath(model.getSimulationRoot(),"StartTime/@value", node.attribute("value"));
	}
	else if (matchPath("MonteCarloSampler/MetropolisKinetics", node)) {
		metropolis_defined = true;
		setPath(model.getSimulationRoot(),"SpatialFramework/CPM/MonteCarloSampler/MetropolisKinetics/@temperature", node.attribute("temperature"));
	}
// neighborhood defaults from lattice (above) get overwritten here
	else if (matchPath("MonteCarloSampler/Neighborhood", node)) {
		auto child = node.firstChildElement("Order");
		if (!child.isNull()) {
			neighbourhoodOrder = child.text();
			neighbourhoodDistance = "";
			return;
		}
		child =  node.firstChildElement("Distance");
		if (!child.isNull()) {
			neighbourhoodDistance = child.text();
			neighbourhoodOrder = "";
			return;
		}
	}
	else if (matchPath("MonteCarloSampler/MCSDuration", node)) {
		setPath(model.getSimulationRoot(),"SpatialFramework/CPM/MonteCarloSampler/MCSDuration/@value", node.attribute("value"));
	}
	else if (matchPath("ShapeSurface",node)) {
		setPath(model.getSimulationRoot(),"SpatialFramework/CPM/ShapeSurface/@scaling", node.attribute("scaling"));
		if ( ! getPath(node, "Neighborhood/Order" ).isNull()) {
			setPath(model.getSimulationRoot(),"SpatialFramework/CPM/ShapeSurface/Neighborhood/Order/@text", getPath(node, "Neighborhood/Order").text());
		}
		if ( ! getPath(node, "Neighborhood/Distance" ).isNull()) {
			setPath(model.getSimulationRoot(),"SpatialFramework/CPM/ShapeSurface/Neighborhood/Distance/@text", getPath(node, "Neighborhood/Distance").text());
		}
	}
}


void PottsRule::finish()
{
	if (neighbourhoodOrder.isNull()) {
		auto distance = neighbourhoodDistance.toDouble();
		QList<double> order_dist;
		if (latticeType == "square") {
			order_dist = {0.0, 1.0, sqrt(2), 2, sqrt(5), sqrt(8), 3, sqrt(10), sqrt(13), 4, sqrt(18)};
		}
		else if (latticeType == "hexagonal" ) {
			order_dist = {0.0, 1.0, 1.5, 2, sqrt(7), 3, sqrt(12), sqrt(13), 4, sqrt(19), sqrt(21)};
		}
		int order=0;
		while ( order_dist.size()-1<order && order_dist[order+1] <= distance ) {
			order++;
		}
		neighbourhoodOrder = QString::number(order);
	}
	
	setPath(model.getSimulationRoot(),"SpatialFramework/CPM/MonteCarloSampler/Neighborhood/Order", neighbourhoodOrder);
//	setPath(model.getSimulationRoot(),"SpatialFramework/CPM/MonteCarloSampler/Neighborhood/Distance", neighbourhoodDistance);
	
}

}
