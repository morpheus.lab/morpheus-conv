
#include <QtCore>
#include <QtXml>
#include <QDebug>

#include "multicell_model.hpp"
#include "../rule_base.hpp"

namespace Morph2MultiCell {
  
class InteractionRule : public XMLRuleBase {
	public: 
		InteractionRule(Model model ) : model(model)  {};
		QList<MatchDesc> matchPaths() override { return {
			{ "Interaction", CONSUMING_NODE},
			{ "Contact", CONSUMING_NODE} };
		}
		void enterMatchingNode(const QDomElement node) override;
		void finish() override {};

	private: 
		Model model;
		QDomElement interaction_list;
		
	
};

void InteractionRule::enterMatchingNode(const QDomElement node) {
	if (node.tagName() == "Interaction" ) {
		if (node.hasAttribute("default")) {
			setPath(model.getModelRoot(),"ListOfInteractions/DefaultAdhesion/@value",node.attribute("default"));
		}
	}
	if (node.tagName() == "Contact") {
		setPath(model.getModelRoot(),
				QString("ListOfInteractions/Adhesion/type1=%1,type2=%2/@value").arg(node.attribute("type1")).arg(node.attribute("type2")),
				node.attribute("value"));
	}
};

}
