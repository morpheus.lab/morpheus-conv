#include "multicell_conv.hpp"

#include "../rule_base.hpp"
#include "compartment_rule.hpp"
#include "interaction_rule.hpp"
#include "space_rule.hpp"
#include "cell_shape_rules.hpp"
#include "potts_rule.hpp"
#include "description_rule.hpp"
#include "property_rule.hpp"

using namespace Morph2MultiCell;

QDomDocument MorpheusML_to_MulticellML(QDomDocument morpheus_doc) {

	Model multicell(morpheus_doc);
	
// Create the converter
	QList<XMLRuleBase*> rules;
	rules.append(new CompartmentRule(multicell));
	rules.append(new InteractionRule(multicell));
	rules.append(new SpaceRule(multicell));
	rules.append(new CellShapeRule(multicell));
	rules.append(new PottsRule(multicell));
	rules.append(new DescriptionRule(multicell));
	rules.append(new PropertyRule(multicell));
// 	rules.append(new TaxisRule(cc3d));
// 	rules.append(new PopulationRule(cc3d));
	
	QMultiMap<QString,XMLRuleBase*> tag_rules;
	for (auto rule : rules) {
		for (auto match : rule->matchPaths()) {
			auto tag = match.path.split("/").last();
			tag_rules.insert(tag, rule);
		}
	}
	XMLRuleBase::processSubTree(morpheus_doc.firstChildElement(), tag_rules);
	
	for (auto rule : rules) {
		rule->finish();
	}

	return multicell.getDocument();
}
