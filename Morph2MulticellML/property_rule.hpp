#include "multicell_model.hpp"
#include "../rule_base.hpp"

namespace Morph2MultiCell {

class PropertyRule :public XMLRuleBase
{
	Model& model;
	int ct_id = 0;

	public:
		PropertyRule(Model& a) : model(a) {};
		QList<MatchDesc> matchPaths() override {
			return {
				{ "Constant", CONSUMING_SUBTREE },
				{ "Variable", CONSUMING_SUBTREE },
				{ "Field"   , CONSUMING_SUBTREE }
			};
		}
		
		void enterMatchingNode(const QDomElement) override;
		void finish() override {};
	private:
		
};

void PropertyRule::enterMatchingNode(const QDomElement node)
{
	if (node.tagName() == "Constant") {
		setPath(model.currentScope()->getMLNode(),QString("Var[symbol=%1]/@value").arg(node.attribute("symbol")),node.attribute("value"));
		setPath(model.currentScope()->getMLNode(),QString("Var[symbol=%1]/@space-const").arg(node.attribute("symbol")), "true");
		setPath(model.currentScope()->getMLNode(),QString("Var[symbol=%1]/@time-const").arg(node.attribute("symbol")),  "true");
		if (node.hasAttribute("name"))
			setPath(model.currentScope()->getMLNode(),QString("Var[symbol=%1]/@name").arg(node.attribute("symbol")),node.attribute("name"));
	}
	if (node.tagName() == "Variable") {
		setPath(model.currentScope()->getMLNode(),QString("Var[symbol=%1]/@value").arg(node.attribute("symbol")),node.attribute("value"));
		setPath(model.currentScope()->getMLNode(),QString("Var[symbol=%1]/@space-const").arg(node.attribute("symbol")), "true");
		setPath(model.currentScope()->getMLNode(),QString("Var[symbol=%1]/@time-const").arg(node.attribute("symbol")),  "false");
		if (node.hasAttribute("name"))
			setPath(model.currentScope()->getMLNode(),QString("Var[symbol=%1]/@name").arg(node.attribute("symbol")),node.attribute("name"));
	}
	if (node.tagName() == "Var") {
		setPath(model.currentScope()->getMLNode(),QString("Var[symbol=%1]/@value").arg(node.attribute("symbol")),node.attribute("value"));
		setPath(model.currentScope()->getMLNode(),QString("Var[symbol=%1]/@space-const").arg(node.attribute("symbol")), "false");
		setPath(model.currentScope()->getMLNode(),QString("Var[symbol=%1]/@time-const").arg(node.attribute("symbol")),  "false");
		if (node.hasAttribute("name"))
			setPath(model.currentScope()->getMLNode(),QString("Var[symbol=%1]/@name").arg(node.attribute("symbol")),node.attribute("name"));
		if ( ! getPath(node, "Diffusion" ).isNull()) {
			setPath(model.currentScope()->getMLNode(),QString("Diffusion[symbol=%1]/@rate").arg(node.attribute("symbol")), getPath(node, "Diffusion").attribute("rate"));
		}
	}
}

}
