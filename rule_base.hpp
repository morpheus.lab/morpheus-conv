#include <QtCore>
#include <QtXml>
// #include <QtXml/QDomElement>
// #include <QtXml/QDomAttr>
#include <QDebug>
#include <QMultiMap>
#ifndef RULE_BASE_HPP
#define RULE_BASE_HPP

class XMLRuleBase 
{
	public :
		/// Return values for node processing
		enum ProcessRet { NONE, NODE_DONE, NODE_USED, SUBTREE_DONE, SUBTREE_PARTIALLY };
		enum Processing { CONSUMING_NODE, USING_NODE, CONSUMING_SUBTREE };
		
		struct MatchDesc {
			QString path;
			Processing processing;
		};

		/// Try to apply conversion rule to node, extracting all data
		virtual QList<MatchDesc> matchPaths() =0;
		/**
		 * Use all the information in the node and convert the content to the new format
		 */
		virtual void enterMatchingNode(const QDomElement node)  =0;
		virtual void leaveMatchingNode(const QDomElement node)  {};
		virtual void finish() =0;
		
		static ProcessRet processSubTree(QDomElement node, const QMultiMap<QString,XMLRuleBase*> & tag_rules);
		
		static void failOnAttributes(QDomElement node, QStringList attributes ) {
			for ( auto attr : attributes)
				if (node.hasAttribute(attr))
					qDebug() << "No rule to convert " << QString("%1/@%2").arg(node.tagName()).arg(attr);
		}
		
		/// match an XPath to a node
		static bool matchPath(QString path, QDomNode node);
		/// get a QDomElement relative to @parent given by an XPath @path
		static QDomElement getPath(QDomElement parent, QString path, bool create = false);
		/// set an Attribute or Text field given by XPath @path to @value
		static bool setPath( QDomElement parent, const QString path, const QString value);
		static bool setPath( QDomElement parent, const QString path, int value) { return setPath(parent,path,QString::number(value)); };
};


#endif
