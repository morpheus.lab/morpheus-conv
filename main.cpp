#include <QtCore>
#include <QtXml>
#include <QDebug>

#include <iostream>
#include "Morph2CC3D/cc3d_conv.hpp"
#include "Morph2Chaste/chaste_conv.hpp"
#include "Morph2MulticellML/multicell_conv.hpp"


int main(int argc, char *argv[]) {
	QCoreApplication a(argc, argv);
	 
	auto args = a.arguments();
	
// 	qDebug() << "received the following cmd line args" << args << endl;
	args.pop_front();
	
	// Parse command line information
	QString input_file;
	QString output_file;
	QStringList output_formats({ "CC3D" , "MorpheusML" , "Chaste" , "MultiCellML"});
	QString output_format;
	
	while ( ! args.isEmpty() ) {
		QString arg = args.first();
		args.pop_front();
		QString key="";
		QString value="";
		if (arg.startsWith("-")) {
			key = arg;
			if (!args.isEmpty()) {
				value = args.front();
				if (value.startsWith("-"))
					 value ="";
				else
					args.pop_front();
			}
		}
		else {
			value = arg;
		}
		
		if (key=="-i" || (args.size()==0 && key == "")) { input_file = value; }
		if (key=="-o") { output_file = value; }
		if (key=="-oformat") { output_format = value; }
	}
	
	qDebug() << "Converting " << input_file << " into "<< output_format;
	
	// validate information
	bool valid = true;
	std::string error;
	if ( !QFileInfo(input_file).exists()) {
		valid = false;
		if (!input_file.isEmpty())
			std::cerr << "Input File does not exist" << std::endl;
	}
	else if ( ! output_formats.contains(output_format) ) {
		std::cerr << "Invalid ouput format specified" << std::endl;
		valid = false;
	}
	QStringList output_representation({"CPM","Vertex","Node"});
	if (!valid) {		
		std::cout << "     <<<  MulticellML converter  >>>" << std::endl;
		std::cout << "Version:" << std::endl  << "  " << PROJECT_VERSION_STRING << std::endl;
		std::cout << "Usage:" << std::endl;
// 		std::cout << "multicellml-conv -i [input] -oformat ["<< output_formats.join("|") << "] [-o output ] ";
		std::cout << "  multicellml-conv [OPTIONS] -oformat output_format input_file" << std::endl;
		std::cout << std::endl;
		std::cout << "Options:" << std::endl;
		std::cout << "  -o           File name of the output model." << std::endl;
		std::cout << "  -oformat     Select output format ("<< output_formats.join("|").toStdString() << ")"<< std::endl;
		std::cout << "  -ocellmodel  Select model for cell representation ("<< output_representation.join("|").toStdString() << ")"<< std::endl;
		std::cout << std::endl;
		return -1;
	}
	
	QDomDocument input;
	QFile file(input_file);
	if (!file.open(QIODevice::ReadOnly)) {
		qDebug() << "Could not read input file";
		return -1;
	}
	if (!input.setContent(&file)) {
		file.close();
		qDebug() << "Could not parse input file";
		return -1;
	}
	QString input_format;
	if (!input.firstChildElement("MorpheusModel").isNull()) {
		input_format = "Morpheus";
	}
	else if (! input.firstChildElement("MulticellML").isNull()) {
		input_format = "MultiCellML";
	}
	else {
		qDebug() << "Could not detect input file format";
		return -1;
	}
	
	file.close();
	
	// Output file name heuristics
	if (output_file.isEmpty()) {
		QString extension;
		if (output_format == "CC3D") extension = "cc3d";
		else if (output_format == "MultiCellML") extension = "multicell";
		else if (output_format == "Morpheus") extension = "morpheus";
		else if (output_format == "Chaste") extension = "chaste.cpp";

		if (input_file.contains('.'))
			output_file = input_file.section(".", 0,-2) + "." + extension;
		else 
			output_file = input_file + "." + extension;
	}
	
	QString output;
	if (input_format == "Morpheus") {
		if (output_format == "CC3D") {
			QString model_output = MorpheusML_to_CC3D(input).toString(2);
			
			QString output_model_file = output_file + ".xml";
			QFile file(output_model_file);
			if (file.open(QIODevice::WriteOnly | QIODevice::Text )) {
				QTextStream stream(&file);
				stream << model_output ;
				file.close();
			}
			else {
				qDebug() << "Unable to open output file " << output_file << " for writing.";
			}
			
			output += "<Simulation version=\"3.6.1\">\n";
			output += "<XMLScript Type=\"XMLScript\">" + output_model_file + "</XMLScript>\n";
			output += "</Simulation>";
		}
		else if (output_format == "MultiCellML") {
			output = MorpheusML_to_MulticellML(input).toString(2);
		}
		else if (output_format == "Chaste") {
			output = MorpheusML_to_Chaste(input).join("\n");
		}
		else {
			qDebug() << "Output format not supported yet";
			return -1;
		}
	}
	else if (input_format == "MultiCellML") {
		if (output_format == "CC3D") {
			QString model_output = "" /* MultiCellML_to_CC3D(input).toString(2) */;
			
			QString output_model_file = output_file + ".xml";
			QFile file(output_model_file);
			if (file.open(QIODevice::WriteOnly | QIODevice::Text )) {
				QTextStream stream(&file);
				stream << model_output ;
				file.close();
			}
			else {
				qDebug() << "Unable to open output file " << output_file << " for writing.";
			}
			
			output += "<Simulation version=\"3.6.1\">\n";
			output += "<XMLScript Type=\"XMLScript\">" + output_model_file + "</XMLScript>\n";
			output += "</Simulation>";
		}
		else if (output_format == "Morpheus") {
			output = "" /* MulticellML_to_MorpheusML(input).toString(2) */ ;
		}
		else if (output_format == "Chaste") {
			output = "" /* MultiCellML_to_Chaste(input).join("\n") */ ;
		}
		else {
			qDebug() << "Output format not supported yet";
			return -1;
		}
	}
	
	
	QFile out_file(output_file);
	if (out_file.open(QIODevice::WriteOnly | QIODevice::Text ))
	{
			QTextStream stream(&out_file);
			stream << output ;
	}
	else {
		qDebug() << "Unable to open output file " << output_file << " for writing.";
	}
	out_file.close();
	
	return 0;
}
