#include <QtCore>
#include <QtXml>
#include <QDebug>

#ifndef CHASTE_MODEL_HPP
#define CHASTE_MODEL_HPP

namespace Chaste {
	
class Model
{
	QStringList header;
	QStringList document;
	QDomElement currRoot;
	QString cellType;
	QString medium_cell_type;
	QString latticeType;
	

	public :
		Model();
		QStringList& getHeader() { return header; }	// We currently load a fixed header section for maximum functionality, will later collect header from actually found processes
		/// Get the Chaste model description
		QStringList& getDocument() { return document; }
		/// Get the name of current CellType
		QString currentCellType() { return cellType; } 
		void setMediumCellType(QString type) { medium_cell_type = type; }
		
// 		void setLatticeType(QString type) { latticeType = type; };
// 		QString getLatticeType() { return latticeType; }
		QString neighborhoodDistance2Order(QString distance);
		
		/// Set the current CellType
		void setCurrentCellType(QString name);
};

}

#endif
