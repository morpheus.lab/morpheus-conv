#include <QtCore>
#include <QtXml>
#include <QDebug>

#include "chaste_model.hpp"
#include "../rule_base.hpp"

namespace Chaste {

class CellTypeRule :public XMLRuleBase
{
	Model& chaste;
	int ct_id = 1;
	bool medium_defined = false;

	public:
		CellTypeRule(Model& a) : chaste(a) {};
		QList<MatchDesc> matchPaths() override { return { {"CellTypes/CellType", CONSUMING_NODE } }; }
		void enterMatchingNode(const QDomElement) override;
		void finish() override;
	private:
		
};

void CellTypeRule::enterMatchingNode(const QDomElement node)
{
	if (matchPath("CellTypes/CellType",node)) {
		if (!medium_defined && node.attribute("class")=="medium") {
			medium_defined=true;
			chaste.setMediumCellType(node.attribute("name"));
			chaste.setCurrentCellType(node.attribute("name"));
		}
// 		setPath(cc3d.currentRoot(),QString("Plugin[Name=CellType]/CellType[TypeId=%1]/@TypeName").arg(ct_id++), node.attribute("name"));
		chaste.getDocument().append(QString("DefineCellType(\"%1\");").arg(node.attribute("name")));
		qDebug() << "defining celltype ";
		chaste.setCurrentCellType(node.attribute("name"));
	}
}

void CellTypeRule::finish() {
	if (!medium_defined) {
// 		setPath(cc3d.currentRoot(),QString("Plugin[Name=CellType]/CellType[TypeId=0]/@TypeName").arg(0), "Medium");
// 		medium_defined=true;
	}
}

}
