#include "chaste_model.hpp"

namespace Chaste {
	
Model::Model()
{
	// prefill with common header 
	QFile file(":/chaste/common_header.txt");
    if(!file.open(QFile::ReadOnly |
                  QFile::Text))
    {
        qDebug() << " Could not open :/chaste/common_header.txt for reading";
        return;
    }
    QTextStream in(&file);
    QString headerText = in.readAll();
    document.append(headerText);
    file.close();
}

QString Model::neighborhoodDistance2Order(QString s_distance)
{
	auto distance = s_distance.toDouble();
	QList<double> order_dist;
	if (latticeType == "Square") {
		order_dist = {0.0, 1.0, sqrt(2), 2, sqrt(5), sqrt(8), 3, sqrt(10), sqrt(13), 4, sqrt(18)};
	}
	else if (latticeType == "Hexagonal") {
		order_dist = {0.0, 1.0, 1.5, 2, sqrt(7), 3, sqrt(12), sqrt(13), 4, sqrt(19), sqrt(21)};
	}
	else {
		qDebug() << "Cannot convert Distance to Order for lattice type '" << latticeType  << "'.";
		return "0";
	}

	int order=0;
	while ( order_dist.size()-1<order && order_dist[order+1] <= distance ) {
		order++;
	}
	return QString::number(order);
}


void Model::setCurrentCellType(QString name)
{
	cellType = name;
}

}




