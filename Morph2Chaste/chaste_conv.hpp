#ifndef CHASTE_CONV_HPP
#define CHASTE_CONV_HPP

#include <QStringList>
#include <QDomDocument>
/// Convert a MorpheusML document to CHASTE
QStringList MorpheusML_to_Chaste(QDomDocument morpheus_doc);

#endif
