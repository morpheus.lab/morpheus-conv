#include "chaste_conv.hpp"

#include "chaste_model.hpp"
#include "../rule_base.hpp"
// #include "potts_rule.hpp"
// #include "contact_rule.hpp"
#include "celltype_rule.hpp"
// #include "cell_shape_rules.hpp"
// #include "field_rule.hpp"
// #include "taxis_rule.hpp"
// #include "population_rule.hpp"



// XMLRuleBase::ProcessRet processSubTree(QDomElement root, QList<XMLRuleBase*> & rules);

QStringList MorpheusML_to_Chaste(QDomDocument morpheus_doc) 
{

	Chaste::Model chaste;
	
// Create the converter
	QList<XMLRuleBase*> rules;
// 	rules.append(new PottsRule(chaste));
	rules.append(new Chaste::CellTypeRule(chaste));
// 	rules.append(new ContactRule(chaste));
// 	rules.append(new CellShapeRule(chaste));
// 	rules.append(new FieldRule(chaste));
// 	rules.append(new TaxisRule(chaste));
// 	rules.append(new PopulationRule(chaste));
	
	QMultiMap<QString,XMLRuleBase*> tag_rules;
	for (auto rule : rules) {
		for (auto match : rule->matchPaths()) {
			auto tag = match.path.split("/").last();
			tag_rules.insert(tag, rule);
		}
	}
	
	XMLRuleBase::processSubTree(morpheus_doc.firstChildElement(), tag_rules);
	
	for (auto rule : rules) {
		rule->finish();
	}

	// append common footer 
	QFile file(":/chaste/common_footer.txt");
	if(!file.open(QFile::ReadOnly |
				QFile::Text))
	{
		qDebug() << " Could not open :/chaste/common_footer.txt";
		return QStringList();
	}
	QTextStream in(&file);
	QString footerText1 = in.readAll();
	QStringList footerText2 = footerText1.split("Lutz");
// 	document.append(footerText1);
	file.close();
	
	return chaste.getHeader() + chaste.getDocument() + footerText2;
}
