#include <QtCore>
#include <QtXml>
#include <QDebug>

#include "cc3d_model.hpp"
#include "../rule_base.hpp"

namespace Morph2CC3D {

class CellTypeRule :public XMLRuleBase
{
	private:
		Model& cc3d;
		int ct_id = 1;
		bool medium_defined = false;

	public:
		CellTypeRule(Model& a) : cc3d(a) {};
	
		QList<MatchDesc> matchPaths() override {
			return {
				{"CellTypes/CellType",  CONSUMING_NODE }
			};
		}
		void enterMatchingNode(const QDomElement) override;
		void finish() override;
};

void CellTypeRule::enterMatchingNode(const QDomElement node)
{
	if (matchPath("CellTypes/CellType",node)) {
		if (!medium_defined && node.attribute("class")=="medium") {
			setPath(cc3d.currentRoot(),QString("Plugin[Name=CellType]/CellType[TypeId=0]/@TypeName"), "Medium");
			medium_defined=true;
			cc3d.setMediumCellType(node.attribute("name"));
			cc3d.setCurrentCellType(node.attribute("name"));
		}
		else {
			setPath(cc3d.currentRoot(),QString("Plugin[Name=CellType]/CellType[TypeId=%1]/@TypeName").arg(ct_id++), node.attribute("name"));
			cc3d.setCurrentCellType(node.attribute("name"));
		}
	}
}

void CellTypeRule::finish() {
	if (!medium_defined) {
		setPath(cc3d.currentRoot(),QString("Plugin[Name=CellType]/CellType[TypeId=0]/@TypeName").arg(0), "Medium");
		medium_defined=true;
	}
}

}
