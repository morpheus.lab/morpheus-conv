#include <QtCore>
#include <QtXml>
#include <QDebug>

#include "cc3d_model.hpp"
#include "../rule_base.hpp"

namespace Morph2CC3D {

class CellShapeRule :public XMLRuleBase
{
	Model& cc3d;
	int ct_id = 0;

	public:
		CellShapeRule(Model& a) : cc3d(a) {};
		QList<MatchDesc> matchPaths() override {
			return {
				{"VolumeConstraint",  CONSUMING_SUBTREE },
				{"SurfaceConstraint", CONSUMING_SUBTREE }
			};
		}
		
		void enterMatchingNode(const QDomElement) override;
		void finish() override {};
	private:
		
};

void CellShapeRule::enterMatchingNode(const QDomElement node)
{
	if (matchPath("VolumeConstraint",node)) {
		
		setPath(cc3d.currentRoot(),
				QString("Plugin[Name=Volume]/VolumeEnergyParameters[CellType=%1]/@LambdaVolume").arg(cc3d.currentCellType()), 
				node.attribute("strength"));
		setPath(cc3d.currentRoot(),
				QString("Plugin[Name=Volume]/VolumeEnergyParameters[CellType=%1]/@TargetVolume").arg(cc3d.currentCellType()),
				node.attribute("target"));
	}
	if (matchPath("SurfaceConstraint",node)) {
		
		setPath(cc3d.currentRoot(),
				QString("Plugin[Name=Surface]/SurfaceEnergyParameters[CellType=%1]/@LambdaSurface").arg(cc3d.currentCellType()), 
				node.attribute("strength"));
		if (node.attribute("mode") == "aspherity") {
			double target = node.attribute("target").toDouble() * 2 * qSqrt(M_PI* node.parentNode().firstChildElement("VolumeConstraint").attribute("target").toDouble());
			setPath(cc3d.currentRoot(),
					QString("Plugin[Name=Surface]/SurfaceEnergyParameters[CellType=%1]/@TargetSurface").arg(cc3d.currentCellType()),
					QString::number(target));
		}
		else 
			setPath(cc3d.currentRoot(),
					QString("Plugin[Name=Surface]/SurfaceEnergyParameters[CellType=%1]/@TargetSurface").arg(cc3d.currentCellType()),
					node.attribute("target"));
	}
}

}

