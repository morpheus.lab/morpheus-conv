#include <QtCore>
#include <QtXml>
#include <QDebug>

#include "cc3d_model.hpp"
#include "../rule_base.hpp"
/*
<Steppable Type="BlobInitializer">
   
   <Region>
      <Center x="50" y="50" z="0"/>
      <Radius>40</Radius>
      <Gap>0</Gap>
      <Width>5</Width>
      <Types>Condensing,NonCondensing</Types>
   </Region>
</Steppable>
*/

namespace Morph2CC3D {
	
class PopulationRule : public XMLRuleBase
{
	Model& cc3d;
	QString neighborhood_order, neighborhood_distance;
	QString scaling;
	public:
		PopulationRule(Model& a) : cc3d(a) {};
		QList<MatchDesc> matchPaths() override {
			return {
				{"Population/InitCircle",  CONSUMING_NODE }
			};
		}
		void enterMatchingNode(const QDomElement) override;
		void finish() override;
		
	private:
		QString scaleEnergy(QString e);
		QDomElement xCircleInit;
		struct CirclePatch {
			QList<int> center;
			uint size;
			double radius;
		};
		QMultiMap<QString,CirclePatch> CirclePops;
		
		int sqr(int a ) { return a*a; };
};

void PopulationRule::enterMatchingNode(const QDomElement node)
{
	if (matchPath("Population/InitCircle",node)) {
		
		failOnAttributes(node, { "mode", "random-displacement" });
		
		CirclePatch patch;
		auto dim_node = node.firstChildElement("Dimensions");
		auto center = dim_node.attribute("center").split(",");
		for (const auto& d :center) patch.center.push_back(d.toInt());
		if (patch.center.size() != 3) {
			qDebug() << "Invalid patch center in <InitCircle> : " << dim_node.attribute("center");
			return;
		}
		patch.radius = dim_node.attribute("radius").toDouble();
		patch.size = node.attribute("number-of-cells").toInt();
		CirclePops.insert(node.parentNode().toElement().attribute("type"), patch);
	}
}

void PopulationRule::finish() {
	if (!CirclePops.isEmpty()) {
		xCircleInit = cc3d.getDocument().createElement("Steppable");
		xCircleInit.setAttribute("Type","BlobInitializer");
		cc3d.currentRoot().appendChild(xCircleInit);
	}
	while (!CirclePops.isEmpty()) {
		QMultiMap<QString, CirclePatch>::iterator i = CirclePops.begin();
		QStringList celltypes (i.key());
		auto patch = i.value();
		i = CirclePops.erase(i);
		
		while (i!=CirclePops.end()) {
			auto o = i.value();
			double centerdist = qSqrt( sqr(patch.center[0]-o.center[0]) + sqr(patch.center[1]-o.center[1]) + sqr(patch.center[2]-o.center[2]));
 			if (centerdist < 10) {
				// merge the patches
				celltypes.append(i.key());
				if (o.radius > patch.radius) patch.radius = o.radius;
				patch.size += o.size;
				i = CirclePops.erase(i);
			}
			else 
				i++;
		}
		
		auto xRegion = cc3d.getDocument().createElement("Region");
		xCircleInit.appendChild(xRegion);
		
		setPath(xRegion, "Center/@x",patch.center[0]);
		setPath(xRegion, "Center/@y",patch.center[1]);
		setPath(xRegion, "Center/@z",patch.center[2]);
		
		setPath(xRegion,"Radius/@text", QString::number(patch.radius));
		setPath(xRegion,"Types/@text", celltypes.join(","));
		setPath(xRegion,"Width/@text", QString::number(qSqrt(M_PI*sqr(patch.radius)/patch.size)));
		setPath(xRegion,"Gap/@text", "0");
	}

}

}



