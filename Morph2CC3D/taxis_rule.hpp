#include "cc3d_model.hpp"
#include "../rule_base.hpp"

namespace Morph2CC3D {

class TaxisRule :public XMLRuleBase
{
	Model& cc3d;
	int ct_id = 0;

	public:
		TaxisRule(Model& a) : cc3d(a) {};
		QList<MatchDesc> matchPaths() override {
			return {
				{"Chemotaxis",  CONSUMING_NODE }
			};
		}
		void enterMatchingNode(const QDomElement) override;
		void finish() override {};
	private:
		
};

/*
 * <ChemotaxisByType Type="Bacterium" Lambda="200" SaturationCoef=”10”/>
 * <Plugin Name="Chemotaxis">
 *   <ChemicalField Source="FlexibleDiffusionSolverFE" Name="FGF">
 *        <ChemotaxisByType Type="Amoeba" Lambda="300"/>
 *         <ChemotaxisByType Type="Bacteria" Lambda="200"/>
 *   </ChemicalField>
 * </Plugin>
*/



void TaxisRule::enterMatchingNode(const QDomElement node)
{
	if (matchPath("Chemotaxis",node)) {
		setPath(cc3d.currentRoot(),
				QString("Plugin[Name=Chemotaxis]/ChemicalField[Source=FlexibleDiffusionSolverFE,Name=%1]/ChemotaxisByType[Type=%2]/@Lambda").arg(node.attribute("field")).arg(cc3d.currentCellType()), 
				node.attribute("strength"));
		
		failOnAttributes(node, { "saturation", "contact-inhibition", "retraction" });
	}
}

}
