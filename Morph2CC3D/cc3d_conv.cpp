#include "cc3d_conv.hpp"

#include "potts_rule.hpp"
#include "contact_rule.hpp"
#include "celltype_rule.hpp"
#include "cell_shape_rules.hpp"
#include "field_rule.hpp"
#include "taxis_rule.hpp"
#include "population_rule.hpp"


using namespace Morph2CC3D;
// XMLRuleBase::ProcessRet processSubTree(QDomElement root, QList<XMLRuleBase*> & rules);

QDomDocument MorpheusML_to_CC3D(QDomDocument morpheus_doc) {

	Model cc3d;
	
// Create the converter
	QList<XMLRuleBase*> rules;
	rules.append(new PottsRule(cc3d));
	rules.append(new CellTypeRule(cc3d));
	rules.append(new ContactRule(cc3d));
	rules.append(new CellShapeRule(cc3d));
	rules.append(new FieldRule(cc3d));
	rules.append(new TaxisRule(cc3d));
	rules.append(new PopulationRule(cc3d));
	
	QMultiMap<QString,XMLRuleBase*> tag_rules;
	for (auto rule : rules) {
		for (auto match : rule->matchPaths()) {
			auto tag = match.path.split("/").last();
			tag_rules.insert(tag, rule);
		}
	}
	
	XMLRuleBase::processSubTree(morpheus_doc.firstChildElement(), tag_rules);
	
	for (auto rule : rules) {
		rule->finish();
	}

	return cc3d.getDocument();
}
