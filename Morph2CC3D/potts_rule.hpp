#include <QtCore>
#include <QtXml>
#include <QDebug>

#include "../rule_base.hpp"
#include "cc3d_model.hpp"

namespace Morph2CC3D {

class PottsRule : public XMLRuleBase
{
	QDomElement xPotts;
	QStringList dim;
	QString latticeType;
	QString neighbourhoodOrder;
	QString neighbourhoodDistance;
	QString stopTime;
	QString time_per_MCS = "1";
	bool cpm_defined = false;
	bool metropolis_defined = false;
	
	Model& cc3d;

	public:
		PottsRule(Model& a) : cc3d(a) { 
			QDomDocument doc = cc3d.getDocument();
			xPotts = doc.createElement("Potts");
		};
		QList<MatchDesc> matchPaths() override {
			return {
				{"Lattice/Size", CONSUMING_NODE},
				{"Lattice", CONSUMING_NODE},
				{"Lattice/Neighborhood", CONSUMING_SUBTREE},
				{"Lattice/BoundaryConditions/Condition", CONSUMING_NODE},
				{"CPM", CONSUMING_NODE}, 
				{"StopTime", CONSUMING_NODE},
				{"MonteCarloSampler/MetropolisKinetics", CONSUMING_NODE},
				{"MonteCarloSampler/Neighborhood", CONSUMING_SUBTREE},
				{"MonteCarloSampler/MCSDuration", CONSUMING_NODE} 
			};
		}
		void enterMatchingNode(const QDomElement) override;
		void finish() override;
};


void PottsRule::enterMatchingNode(const QDomElement node)
{
	QDomElement elem=node;
	
	if (matchPath("Lattice/Size", node))
	{
		if (!node.attributes().contains("value")) {
			qDebug() << "Missing lattice size value !";
			dim << "100" << "100" << "100";
		}
		else { 
			dim = node.attribute("value").split(",");
		}
		
		for (auto& d :dim) d=d.trimmed();
		
		setPath(xPotts,"Dimensions/@x",  dim[0] );
		setPath(xPotts,"Dimensions/@y",  dim[1] );
		setPath(xPotts,"Dimensions/@z", (dim[2].toInt()<1 ? "1" :dim[2]) );
	}
	else if (matchPath("Lattice", node))
	{
		
		latticeType = node.attribute("class","square"); 
		if (latticeType == "square")
			latticeType = "Square";
		else if (latticeType == "hexagonal")
			latticeType = "Hexagonal";
		else if (latticeType == "cubic") {
			latticeType = "Cubic";
		}
		setPath(xPotts,"LatticeType",latticeType);
		cc3d.setLatticeType(latticeType);
	}
	else if (matchPath("Lattice/Neighborhood",node)) {
		if (! (neighbourhoodOrder.isEmpty() && neighbourhoodDistance.isEmpty()) )
			return;
		auto child = node.firstChildElement("Order");
		if ( ! child.isNull() ) {
			neighbourhoodOrder = node.text();
			return;
		}
		child = node.firstChildElement("Distance");
		if  (! child.isNull() ) {
			neighbourhoodDistance = node.text();
		}
	}
	else if (matchPath("Lattice/BoundaryConditions/Condition",node)) {
		if (node.attribute("type") == "periodic") {
			if (node.attribute("boundary").contains("x")) {
				setPath(xPotts,"Boundary_x", "Periodic");
			}
			if (node.attribute("boundary").contains("y")) {
				setPath(xPotts,"Boundary_y", "Periodic");
			}
			if (node.attribute("boundary").contains("z")) {
				setPath(xPotts,"Boundary_z", "Periodic");
			}
		}
	}
	else if (matchPath("CPM", node)) {
		cpm_defined = true;
	}
	else if (matchPath("StopTime",node)) {
		stopTime = node.attribute("value");
	}
	else if (matchPath("MonteCarloSampler/MetropolisKinetics", node)) {
		metropolis_defined = true;
		setPath(xPotts,"Temperature", node.attribute("temperature"));
	}
	else if (matchPath("MonteCarloSampler/Neighborhood/Order", node)) {
		neighbourhoodOrder = node.text();
		neighbourhoodDistance = "";
	}
	else if (matchPath("MonteCarloSampler/Neighborhood/Distance", node)) {
		neighbourhoodDistance = node.text();
		neighbourhoodOrder = "";
	}
	else if (matchPath("MonteCarloSampler/MCSDuration", node)) {
		time_per_MCS = node.attribute("value");
	}
}


void PottsRule::finish()
{
	setPath(xPotts,"Steps", QString::number( int(stopTime.toDouble() / time_per_MCS.toDouble()) ) );
	
	if (neighbourhoodOrder.isEmpty()) {
		auto distance = neighbourhoodDistance.toDouble();
		QList<double> order_dist;
		if (latticeType == "square") {
			order_dist = {0.0, 1.0, sqrt(2), 2, sqrt(5), sqrt(8), 3, sqrt(10), sqrt(13), 4, sqrt(18)};
		}
		else if (latticeType == "hexagonal" ) {
			order_dist = {0.0, 1.0, 1.5, 2, sqrt(7), 3, sqrt(12), sqrt(13), 4, sqrt(19), sqrt(21)};
		}
		int order=0;
		while ( order_dist.size()-1<order && order_dist[order+1] <= distance ) {
			order++;
		}
		neighbourhoodOrder = QString::number(order);
	}
	
	setPath(xPotts, "NeighborOrder", neighbourhoodOrder);
	
	if (cpm_defined) {
		auto roo = cc3d.currentRoot();
		roo.insertBefore(xPotts,roo.firstChildElement());
	}
}

}
