#ifndef CC3D_CONV_HPP
#define CC3D_CONV_HPP

#include "cc3d_model.hpp"

/// Convert a MorpheusML document to CC3D
QDomDocument MorpheusML_to_CC3D(QDomDocument morpheus_doc);

#endif
