#include <QtCore>
#include <QtXml>
#include <QDebug>

#ifndef CC3D_MODEL_HPP
#define CC3D_MODEL_HPP

namespace Morph2CC3D {
	
class Model
{
	QDomDocument document;
	QDomElement currRoot;
	QString cellType;
	QString medium_cell_type;
	QString latticeType;
	

	public :
		Model();
		/// Get the CC3D XML model description
		QDomDocument getDocument() { return document; }
		/// Get the name of current CellType
		QString currentCellType() { return cellType; } 
		void setMediumCellType(QString type) { medium_cell_type = type; }
		QString translateCellType(QString celltype) { if (celltype == medium_cell_type) return "Medium"; else if (celltype.isEmpty()) return cellType; else return celltype; } 
		
		void setLatticeType(QString type) { latticeType = type; };
		QString getLatticeType() { return latticeType; }
		QString neighborhoodDistance2Order(QString distance);
		
		/// Set the current CellType
		void setCurrentCellType(QString name);
		/// Get the current Node of the document
		QDomElement currentRoot() { return currRoot; }
		/// Set the current Node of the document
		void setCurrentRoot(QDomElement &);
};

}

#endif
