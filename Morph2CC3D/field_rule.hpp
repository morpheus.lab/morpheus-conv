
#include <QtCore>
#include <QtXml>
#include <QDebug>

#include "cc3d_model.hpp"
#include "../rule_base.hpp"

namespace Morph2CC3D {

class FieldRule :public XMLRuleBase
{
	Model& cc3d;
	QDomElement xSteppable;
	int ct_id = 0;

public:
	FieldRule(Model& a) : cc3d(a) {};
	QList<MatchDesc> matchPaths() override {
			return {
// 				{"Field",  CONSUMING_NODE },
				{"Field/Diffusion",  CONSUMING_NODE }
			};
		}
	void enterMatchingNode(const QDomElement) override;
	void finish() override {};
private:

};

void FieldRule::enterMatchingNode(const QDomElement node)
{
//     if (matchPath("Field",node)) {
// 		
// 		return XMLRuleBase::NODE_USED;
// 	}
	if (matchPath("Field/Diffusion",node)) {
		if (xSteppable.isNull()) {
			xSteppable = cc3d.getDocument().createElement("Steppable");
			xSteppable.setAttribute("Type","FlexibleDiffusionSolverFE");
			xSteppable.appendChild(cc3d.getDocument().createElement("AutoscaleDiffusion"));
			cc3d.currentRoot().appendChild(xSteppable);
		}
		
		auto field_node = node.parentNode().toElement();
		auto xDiffusion = cc3d.getDocument().createElement("DiffusionField");
		xDiffusion.setAttribute("Name", field_node.attribute("symbol"));
		setPath(xDiffusion,"InitialConcentrationExpression/@text", field_node.attribute("value"));
		
		setPath( xDiffusion,"Fieldname/@text", (field_node.attribute("name").isEmpty() ? field_node.attribute("symbol") : field_node.attribute("name")) );
		setPath( xDiffusion,"DiffusionConstant/@text", node.attribute("rate") );
		setPath( xDiffusion,"DeltaX/@text", "1.0");
		xSteppable.appendChild(xDiffusion);
	}
}

}

/**
 * <Steppable Type="FlexibleDiffusionSolverFE">
    <AutoscaleDiffusion/>
    <DiffusionField Name="FGF8">
        <DiffusionData>
            <FieldName>FGF8</FieldName>
            <DiffusionConstant>0.1</DiffusionConstant>
            <DecayConstant>0.002</DecayConstant>
            <ExtraTimesPerMCS>5</ExtraTimesPerMCS>
            <DeltaT>0.1</DeltaT>
            <DeltaX>1.0</DeltaX>
            <DoNotDiffuseTo>Bacteria</DoNotDiffuseTo>
            <InitialConcentrationExpression>x*y
            </InitialConcentrationExpression>
        </DiffusionData>

        <SecretionData>
            <Secretion Type="Amoeba">0.1</Secretion>
        </SecretionData>

        <BoundaryConditions>
            <Plane Axis="X">
                <ConstantValue PlanePosition="Min" Value="10.0"/>
                <ConstantValue PlanePosition="Max"  Value="10.0"/>
            </Plane>

            <Plane Axis="Y">
                <ConstantDerivative PlanePosition="Min" Value="10.0"/>
                <ConstantDerivative PlanePosition="Max"  Value="10.0"/>
            </Plane>
        </BoundaryConditions>

    </DiffusionField>

    <DiffusionField Name="FGF">
        <DiffusionData>
            <FieldName>FGF</FieldName>
            <DiffusionConstant>0.02</DiffusionConstant>
            <DecayConstant>0.001</DecayConstant>
            <DeltaT>0.01</DeltaT>
            <DeltaX>0.1</DeltaX>
            <DoNotDiffuseTo>Bacteria</DoNotDiffuseTo>
        </DiffusionData>
        <SecretionData>
            <SecretionOnContact Type="Medium"
             SecreteOnContactWith="Amoeba">0.1</SecretionOnContact>
            <Secretion Type="Amoeba">0.1</Secretion>
        </SecretionData>
    </DiffusionField>
</Steppable>
**/
