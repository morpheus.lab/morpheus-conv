#include <QtCore>
#include <QtXml>
#include <QDebug>

#include "cc3d_model.hpp"
#include "../rule_base.hpp"

namespace Morph2CC3D {

class ContactRule : public XMLRuleBase
{
	Model& cc3d;
	QString neighborhood_order, neighborhood_distance;
	QDomElement xContact;
	QString scaling;
	public:
		ContactRule(Model& a) : cc3d(a) {
			xContact = cc3d.getDocument().createElement("Plugin");
			xContact.setAttribute("Name","Contact");
		};
		QList<MatchDesc> matchPaths() override {
			return {
				{"ShapeSurface",  CONSUMING_NODE },
				{"ShapeSurface/Neighborhood",  CONSUMING_SUBTREE },
				{"Interaction/Contact",  CONSUMING_NODE }
			};
		}
		void enterMatchingNode(const QDomElement) override;
		void finish() override;
		
	private:
		QString scaleEnergy(QString e);
};

void ContactRule::enterMatchingNode(const QDomElement node)
{
	if (matchPath("ShapeSurface",node)) {
		scaling = node. attribute("scaling");
	}
	else if (matchPath("ShapeSurface/Neighborhood/Order", node )) {
		neighborhood_order = node.text();
	}
	else if (matchPath("ShapeSurface/Neighborhood/Distance",node)) {
		neighborhood_distance = node.text();
	}
	else if (matchPath("Interaction/Contact",node)) {
		 auto xEnergy = cc3d.getDocument().createElement("Energy");
		 setPath(xEnergy,"@Type1", cc3d.translateCellType(node.attribute("type1")) );
		 setPath(xEnergy,"@Type2", cc3d.translateCellType(node.attribute("type2")) );
		 setPath(xEnergy,"@text", scaleEnergy(node.attribute("value")));
		 xContact.appendChild(xEnergy);
	}
}

QString ContactRule::scaleEnergy(QString e)
{
	
	QMap<QString, QList<double> > magno_scaling_by_order =
	{ { "Linear",    {0,1,2,3,4} },
	  { "Square",    {0,1,3,5,11,15,18,26,36} },
	  { "Hexagonal", {0,2.2,6,10.4,22.1,28.65} },
	  { "Cubic",     {0,1,5,9,11,23,39,47,70} }
	};
	
	QMap<QString, QList<double> > size_scaling_by_order =
	{ { "Linear",    {0,2,4,6,8} },
	  { "Square",    {0,4,8,12,16,20,24,28,30} },
	  { "Hexagonal", {0,6,12,18,30,36,42,54,60,72} },
	  { "Cubic",     {0,6,18,26,32,56,80,92,122,130} }
	};
	
	if (scaling == "size") {
		return QString::number(e.toDouble() / size_scaling_by_order[cc3d.getLatticeType()][neighborhood_order.toInt()]);
	}
	else if (scaling == "magno" || scaling == "norm" || scaling == "normalized") {
		return QString::number(e.toDouble() / magno_scaling_by_order[cc3d.getLatticeType()][neighborhood_order.toInt()]);
	}
	else
		return e;
}

void ContactRule::finish()
{
	if (xContact.hasChildNodes()) {
		auto el = xContact.firstChildElement("Energy");
		while ( ! el.isNull()) {
			setPath(el,"@text", scaleEnergy(el.text()) );
			el = el.nextSiblingElement();
		}
		
		if (!neighborhood_order.isEmpty()) {
			setPath(xContact, "NeighborOrder/@text", neighborhood_order );
		}
		else if  (!neighborhood_distance.isEmpty()) {
			setPath(xContact, "NeighborOrder/@text", cc3d.neighborhoodDistance2Order(neighborhood_distance) );
		}
		
		cc3d.currentRoot().appendChild(xContact);
	}
}

}


