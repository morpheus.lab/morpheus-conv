#include "rule_base.hpp"


/// Match a XPath
bool XMLRuleBase::matchPath(QString path, QDomNode node) {
// 	qDebug() << "matching " << path;
	bool full_path = path.startsWith("/");
	auto tags = path.split("/");
	auto current_node = node;
	QString attrib = "";
	if (tags.last().startsWith("@")) {
		attrib = tags.last();
		attrib.remove(0,1);
		tags.removeLast();
		if (attrib == ("text")) {
			if (!current_node.isText())
				return false;
			current_node = current_node.parentNode();
		}
		else {
			// match an attribute node;
			if (!current_node.isAttr() || current_node.nodeName() !=  attrib)
				return false;
		}
	}
	
	while ( ! tags.isEmpty() ) {
		auto parts = tags.last().split("[");
		QString tag = parts[0];
		
		if (tag != current_node.nodeName()) 
			return false;
		
		if (parts.size()>1) {
			QStringList raw_constraint;
			raw_constraint = parts[1].remove("]").split(",");
			QMap<QString,QString> constraint;
			for (auto c : raw_constraint) {
				auto t = c.split("=");
				if (t.size() != 2) {
					qDebug() << "Error matching constraint "<< c <<" in node  " << tags.first();
					continue;
				}
				auto key = t[0].remove('"').remove('\'');
				auto value = t[1].remove('"').remove('\'');
				constraint[key]=value;
			}
			
			bool all = true;
			auto c=constraint.constBegin();
			while (c!=constraint.constEnd()) {
				if (current_node.toElement().attribute(c.key()) != c.value()) {
					all = false;
					break;
				}
				c++;
			}
			
			if (!all)
				return false;
		}
		
		// Node matched; move up the path
		tags.removeLast();
		current_node = current_node.parentNode();
	}
	
	if (!tags.isEmpty())
		return false; 
	
	if (full_path && !current_node.isNull())
		return false;
	
	return true;
}
		
QDomElement XMLRuleBase::getPath(QDomElement parent, QString path, bool create) {
	
	if (path.isEmpty())
		return parent;
	
	auto current_node = parent;
	auto tags = path.split("/");
	while ( ! tags.isEmpty() ) {
		
		auto parts = tags.first().split("[");
		QString tag = parts[0];
		if (parts.size()>1) {
			
			QStringList raw_constraint;
			raw_constraint = parts[1].remove("]").split(",");
			QMap<QString,QString> constraint;
			for (auto c : raw_constraint) {
				auto t = c.split("=");
				if (t.size() != 2) {
					qDebug() << "Error matching constraint "<< c <<" in node  " << tags.first();
					continue;
				}
				auto key = t[0].remove('"').remove('\'');
				auto value = t[1].remove('"').remove('\'');
				constraint[key]=value;
			}
			
			auto child = current_node.firstChildElement(tag);
			while (!child.isNull()) {
				bool all = true;
				auto c=constraint.constBegin();
				while (c!=constraint.constEnd()) {
					if (child.attribute(c.key()) != c.value()) {
						all = false;
						break;
					}
					c++;
				}
				if (all)
					break;
				
				child = child.nextSiblingElement(tag);
			}
			
			if (child.isNull()) {
				// no suitable child found --> create one
				child = current_node.ownerDocument().createElement(tag);
				auto c=constraint.constBegin();
				while (c!=constraint.constEnd()) {
					child.setAttribute(c.key(),c.value());
					c++;
				}
				current_node.appendChild(child);
			}
			
			current_node = child;
			
		}
		else {
			if (current_node.firstChildElement(tag).isNull()) {
				if (!create) return QDomElement();
				auto child = current_node.ownerDocument().createElement(tag);
				current_node.appendChild(child);
				current_node = child;
			}
			else {
				current_node =  current_node.firstChildElement(tag);
			}
		}
			
			
		// Node found; move down the path
		tags.removeFirst();
	}
	return current_node;
}
		
bool XMLRuleBase::setPath( QDomElement parent, QString path, QString value) {
// 	qDebug() << "Setting Path " << path;
	QString attrib = path.section("/",-1);
	if (attrib.startsWith("@")) {
		attrib.remove(0,1);
		int attr_idx = path.lastIndexOf("/");
		if (attr_idx<0) path = "";
		else path.truncate(attr_idx);
	}
	else {
		attrib = "text";
	}
	
	auto current_node = getPath(parent, path, true);
	
	if (attrib != "text") {
		current_node.setAttribute(attrib,value);
	}
	else {
		QDomNode text_node = current_node.firstChild();
		auto childNodes =  current_node.childNodes();
		for (uint i=0; i<childNodes.size(); i++) {
			if (childNodes.item(i).isText()) {
				text_node = childNodes.item(i).toText();
				break;
			}
		}
		if (text_node.isNull()) {
			current_node.appendChild( current_node.ownerDocument().createTextNode(value) );
		}
		else {
			text_node.setNodeValue(value);
		}
	}
	
	return true;
}


/**
 * Process a XML Node and it's child nodes
 * Returns either SUBTREE_DONE, SUBTREE_PARTIALLY or NONE
 **/
XMLRuleBase::ProcessRet XMLRuleBase::processSubTree(QDomElement node, const QMultiMap<QString,XMLRuleBase*> & tag_rules)
{
// 	qDebug() << "Processing " << node.tagName();
	if(node.isNull()) return XMLRuleBase::SUBTREE_DONE;
	   XMLRuleBase::ProcessRet processing_result = XMLRuleBase::NONE;
	
	QList<XMLRuleBase*>  rules  = tag_rules.values(node.nodeName());
	for(int i=0;i<rules.size();)
	{
		// fine grained matching
		bool matching = false;
		Processing rule_processing;
		for ( auto match : rules[i]->matchPaths() ) {
			if (XMLRuleBase::matchPath(match.path, node)) {
				matching = true;
				rule_processing = match.processing;
			}
		}
		if (!matching) {
			rules.removeAt(i);
			continue;
		}
		
		rules[i]->enterMatchingNode(node);
		
		if ( processing_result == NODE_DONE ) {
			if (rule_processing == CONSUMING_SUBTREE) {
				processing_result = SUBTREE_DONE;
			}
		}
		else if ( processing_result == NODE_USED ) {
			if (rule_processing == CONSUMING_SUBTREE) {
				processing_result = SUBTREE_DONE;
			}
			else if (rule_processing == CONSUMING_NODE) {
				processing_result = NODE_DONE;
			}
		}
		else if ( processing_result == NONE ) {
			if (rule_processing == CONSUMING_SUBTREE) {
				processing_result = SUBTREE_DONE;
			}
			else if (rule_processing == CONSUMING_NODE) {
				processing_result = NODE_DONE;
			}
			else if (rule_processing == USING_NODE) {
				processing_result = NODE_USED;
			}
		}
		i++;
	}
	
	// if the whole subtree was already converted we can skip child processing
	if (processing_result == XMLRuleBase::SUBTREE_DONE) {
		return XMLRuleBase::SUBTREE_DONE;
	}
	
	if (processing_result == XMLRuleBase::NODE_USED)
		processing_result = XMLRuleBase::NODE_DONE;
	
	auto child = node.firstChildElement();
	if ( ! child.isElement() ) {
		if (processing_result == XMLRuleBase::NODE_DONE)
			processing_result = XMLRuleBase::SUBTREE_DONE;
		else
			processing_result = XMLRuleBase::NONE;
	}
	else {
		// Process Child nodes 
		QStringList unprocessed_nodes;
		XMLRuleBase::ProcessRet child_processing = processSubTree(child, tag_rules);
		if (child_processing == XMLRuleBase::NONE) 
			unprocessed_nodes.append(child.tagName());
		
		child = child.nextSiblingElement();
		
		while ( child.isElement() ) {
			
			switch (processSubTree(child, tag_rules )) {
				case XMLRuleBase::SUBTREE_DONE :
					if (child_processing == XMLRuleBase::NONE)
						child_processing = XMLRuleBase::SUBTREE_PARTIALLY;
					break;
				case XMLRuleBase::SUBTREE_PARTIALLY :
					child_processing = XMLRuleBase::SUBTREE_PARTIALLY;
					break;
				case XMLRuleBase::NONE:
					unprocessed_nodes.append(child.tagName());
					if (child_processing == XMLRuleBase::SUBTREE_DONE)
						child_processing = XMLRuleBase::SUBTREE_PARTIALLY;
					break;
				default:
					break;
			}
			child = child.nextSiblingElement();
		}
		
		if (child_processing == XMLRuleBase::NONE && processing_result == XMLRuleBase::NODE_DONE)
			processing_result = XMLRuleBase::SUBTREE_PARTIALLY;
		else 
			processing_result = child_processing;
		
		if (processing_result != XMLRuleBase::NONE && ! unprocessed_nodes.isEmpty()) {
			qDebug() << "No rule to convert "
			         << QString("%1/[%2]").arg(node.tagName()).arg(unprocessed_nodes.join(", ")); 
		}
		
	}
	
	// Notify leaving the node
	for(int i=0;i<rules.size();i++) {
		rules[i]->leaveMatchingNode(node);
	}
	
	return processing_result;
}


