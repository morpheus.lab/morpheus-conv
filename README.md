# Multi-Cellular Model Converter

The converter is a command line tool that comprises a collection of rule sets that will allow to convert between decalarative modelling languages and to imperative modelling frameworks.

For further information look at [https://multicellml.org](https://multicellml.org)

<img src="https://multicellml.org/wiki/lib/exe/fetch.php?media=logo.png" width="100" />

## Target formats

Currently supported output formats are
   - **[MultiCellML](https://multicellml.org)** the interoperable exchange format for multi-cellular modelling.
   - **MorpheusML** native language of the [Morpheus](https://morpheus.gitlab.io) modelling framework.
   - **CC3D** language used be the [CompuCell3D](www.compucell3d.org) framework.
   - **Chaste** simulation sources for the [Chaste](https://www.cs.ox.ac.uk/chaste/index.html) framework.

## Building 
### Dependencies

  - CMake>=3.1
  - g++(>=c++11)
  - Qt5
  
On debian related systems run
```
  apt-get install  build-essentials cmake git qtbase5-dev
```

### Instructions
Check out the source
```
   git clone https://gitlab.com/morpheus.lab/morpheus-conv.git multicell-conv
   cd multicell-conv
   git checkout develop
```
and build ...
```
   mkdir build && cd build
   cmake ..
   make 
```
